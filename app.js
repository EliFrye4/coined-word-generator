/*
Copyright (c) 2018 Eli Frye

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (The Coined Word Generator), to deal
in The Coined Word Generator without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of The Coined Word Generator, and to permit persons to whom The Coined Word Generator is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of The Coined Word Generator.

THE COINED WORD GENERATOR IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE COINED WORD GENERATOR OR THE USE OR OTHER DEALINGS IN THE
THE COINED WORD GENERATOR.
*/
var letters = { 
	"initial"  :  ["b","c","d","f","g","h","j","k","l","m","n","p","r","s","t","v","w","x","y","z"], 
	"initialMulti"  :  ["qu","sc","ch","gh","ph","rh","sh","ti","th","wh","zh","ci","wr","qu","bl","br","cr","dr","fl","fr","gl","gr","pr","sk","sl","sm","sn","sp","st","sw","tr","tw","sch","scr","shr","spl","spr","squ","str","thr","ll"], 
	"nucleus"   :  ["a","e","i","o","u","y"],
	"nucleusMulti"   :  ["ai","au","ei","eu","oi","ou","aw","ay","ea","ew","ey","ie","oo","ou","ow","oy"],
	"final" :  ["b","c","d","f","g","h","j","k","l","m","n","p","r","s","t","v","w","x","y","z"],
	"finalMulti" :  ["qu","sc","ng","ch","ck","gh","ph","rh","th","sk","sm","sp","st","nth","sch","ngst","ngsts","tch","ll"]
}
var oddsOfAddingAnotherSyllable = 0.6
// A syllable has the structure: 1. optional initial consonant ( may be simple or c) 2. nucleus ( may be single or multicharacter) 3.  optional final consonant ( may be single or multicharacter) 
var oddsOfHavingInitial = 0.8
var oddsOfHavingFinal = 0.8
var oddsOfSingleCharacterInitial = 0.7
var oddsOfSingleCharacterNucleus = 0.9
var oddsOfSingleCharacterFinal = 0.7
var minimumNumberOfSyllables = 1;
var maximumNumberOfSyllables = 1000;
var advancedShown = 0 // Initially advanced features are not shown.
var errors = []
var numberOfWords = 0
$(document).ready(function(){
	$( "#generate" ).click(function() {
		if(advancedShown) saveAdvancedSettings()
		if ($('#clear').is(':checked')) $( "#coined-word-results" ).empty(); // Clear the previously gnerated words if the user indicated to do so.
		numberOfWords = $('#number-of-words').val() != "" ? $('#number-of-words').val() : 1 // If the user doesn't specify how many words to generate, we'll generate 1
		numberOfWords = numberOfWords >=0 && numberOfWords % 1 == 0 ? numberOfWords : -1 // Check that number of words to be generated is a non-negative integer.
		errors.push({"condition" : "numberOfWords == -1", "text" : "Enter a non-negative integer"})
		errors.push({"condition" : "minimumNumberOfSyllables > maximumNumberOfSyllables", "text" : "Minimum number of syllables cannot be greater than maximum number of syllables"})
		errors.push({"condition" : "minimumNumberOfSyllables < 0", "text" : "Minimum number of syllables cannot less than 0"})
		errors.push({"condition" : "oddsOfHavingInitial < 0 || oddsOfHavingInitial > 1",  "text" :"Odds of a syllable having an initial consonant must be between 0 and 1 inclusive."})
		errors.push({"condition" : "oddsOfHavingFinal < 0 || oddsOfHavingFinal > 1", "text" : "Odds of a syllable having an final consonant must be between 0 and 1 inclusive."})
		errors.push({"condition" : "oddsOfSingleCharacterInitial < 0 || oddsOfSingleCharacterInitial > 1", "text" : "Odds of a initial consonant being a single character must be between 0 and 1 inclusive."})
		errors.push({"condition" : "oddsOfSingleCharacterNucleus < 0 || oddsOfSingleCharacterNucleus > 1", "text" : "Odds of a vowel being a single character must be between 0 and 1 inclusive."})
		errors.push({"condition" : "oddsOfSingleCharacterFinal < 0 || oddsOfSingleCharacterFinal > 1", "text" : "Odds of a final consonant being a single character must be between 0 and 1 inclusive."})
		errors.push({"condition" : "oddsOfAddingAnotherSyllable < 0 || oddsOfAddingAnotherSyllable > 1", "text" : "Odds of adding another syllable to word must be between 0 and 1 inclusive."})
		errors.push({"condition" : "minimumNumberOfSyllables <= 0 == -1", "text" : "Minimum number of syllables must be greater than or equal to 1 inclusive."})
		errors.push({"condition" : "letters.initial.length == 1 && letters.initial[0] == ''", "text" : "List of single character initial consonants cannot be empty."})
		errors.push({"condition" : "letters.initialMulti.length == 1 && letters.initialMulti[0] == ''", "text" : "List of multiple character initial consonants cannot be empty."})
		errors.push({"condition" : "letters.final.length == 1 && letters.final[0] == ''", "text" : "List of single character final consonants cannot be empty."})
		errors.push({"condition" : "letters.finalMulti.length == 1 && letters.finalMulti[0] == ''", "text" : "List of multiple character final consonants cannot be empty."})
		errors.push({"condition" : "letters.nucleus.length == 1 && letters.nucleus[0] == ''", "text" : "List of single character vowels cannot be empty."})
		errors.push({"condition" : "letters.nucleusMulti.length == 1 && letters.nucleusMulti[0] == ''", "text" : "List of multiple character vowels cannot be empty."})
		errors.push({"condition" : "!trueForAll(letters.nucleusMulti, function(str){return str.length > 1})", "text" : "Multicharacter vowels must contain more than one character"})
		errors.push({"condition" : "!trueForAll(letters.initialMulti, function(str){return str.length > 1})", "text" : "Multicharacter initial consonants must contain more than one character"})
		errors.push({"condition" : "!trueForAll(letters.finalMulti, function(str){return str.length > 1})", "text" : "Multicharacter final consonants must contain more than one character"})
		errors.push({"condition" : "!trueForAll(letters.nucleus, function(str){return str.length == 1})", "text" : "Single character vowels must contain one character"})
		errors.push({"condition" : "!trueForAll(letters.initial, function(str){return str.length == 1})", "text" : "Single character initial consonants must contain one character"})
		errors.push({"condition" : "!trueForAll(letters.final, function(str){return str.length == 1})", "text" : "Single character final must contain one character"})
		evaluateIfNoErrorsOtherwiseDisplayErrorMessage(errors, generateWords, "#coined-word-results")
	});
	$( "#show-advanced" ).click(function() {
		advancedShown = !advancedShown
		if(!advancedShown) saveAdvancedSettings()
		var buttonText = advancedShown ? 'Hide advanced options' : 'Show advanced options'
		$("#show-advanced").text(buttonText)
		$("#advanced-options").empty()
		if(advancedShown)
		{
			$("#advanced-options").append("<label for='minimum-number-of-syllables'>Minimum number of syllables:</label> <input type='number' step='1' class='form-control' id='minimum-number-of-syllables'>")
			$("#advanced-options").append("<label for='maximum-number-of-syllables'>Maximum number of syllables:</label> <input type='number' step='1' class='form-control' id='maximum-number-of-syllables'>")
			$("#advanced-options").append("<label for='odds-of-adding-another-syllable'>Odds of adding another syllable:</label> <input type='number' step='.01' class='form-control' id='odds-of-adding-another-syllable'>")
			$("#advanced-options").append("<label for='odds-of-having-initial'>Odds of having initial consonant in syllable:</label> <input type='number' step='.01' class='form-control' id='odds-of-having-initial'>")
			$("#advanced-options").append("<label for='odds-of-having-final'>Odds of having final consonant in syllable:</label> <input type='number' step='.01' class='form-control' id='odds-of-having-final'>")
			$("#advanced-options").append("<label for='odds-of-single-character-initial'>Odds of single character initial:</label> <input type='number' step='.01' class='form-control' id='odds-of-single-character-initial'>")
			$("#advanced-options").append("<label for='odds-of-single-character-final'>Odds of single character final:</label> <input type='number' step='.01' class='form-control' id='odds-of-single-character-final'>")
			$("#advanced-options").append("<label for='odds-of-single-character-nucleus'>Odds of single character nucleus:</label> <input type='number' step='.01' class='form-control' id='odds-of-single-character-nucleus'>") 
			$("#advanced-options").append("<label for='initial'>Single character initial consonants:</label> <input type='text' class='form-control' id='initial'>")
			$("#advanced-options").append("<label for='final'>Single character final consonants:</label> <input type='text' class='form-control' id='final'>") 
			$("#advanced-options").append("<label for='nucleus'>Single character vowels:</label> <input type='text' class='form-control' id='nucleus'>")
			$("#advanced-options").append("<label for='initial-multi'>Multi character initial consonants:</label> <input type='text' class='form-control' id='initial-multi'>") 
			$("#advanced-options").append("<label for='final-multi'>Multi character final consonants:</label> <input type='text' class='form-control' id='final-multi'>")
			$("#advanced-options").append("<label for='nucleus-multi'>Multi character vowels:</label> <input type='text' class='form-control' id='nucleus-multi'>")    
			
			$("#odds-of-adding-another-syllable").attr("value", oddsOfAddingAnotherSyllable);
			$("#odds-of-having-initial").attr("value", oddsOfHavingInitial);
			$("#odds-of-having-final").attr("value", oddsOfHavingFinal);
			$("#odds-of-single-character-initial").attr("value", oddsOfSingleCharacterInitial);
			$("#odds-of-single-character-final").attr("value", oddsOfSingleCharacterFinal);
			$("#odds-of-single-character-nucleus").attr("value", oddsOfSingleCharacterNucleus);
			$("#minimum-number-of-syllables").attr("value", minimumNumberOfSyllables);
			$("#maximum-number-of-syllables").attr("value", maximumNumberOfSyllables);
			$("#initial").attr("value", letters.initial.toString());
			$("#final").attr("value", letters.final.toString());
			$("#nucleus").attr("value", letters.nucleus.toString());
			$("#initial-multi").attr("value", letters.initialMulti.toString());
			$("#final-multi").attr("value", letters.finalMulti.toString());
			$("#nucleus-multi").attr("value", letters.nucleusMulti.toString());
		}
	});
})


function determineNumberOfSyllables()
{
	var numberOfSyllables = $('#minimum-number-of-syllables').length && $('#minimum-number-of-syllables').val() != "" ? $('#minimum-number-of-syllables').val() : 1
	//var numberOfSyllables = 1
	while(randomBool(oddsOfAddingAnotherSyllable)){
		if( $('#maximum-number-of-syllables').length && $('#maximum-number-of-syllables').val() != "" && numberOfSyllables == $('#maximum-number-of-syllables').val() ) break
		numberOfSyllables++
	}
	return numberOfSyllables
}
// if odds is 0.8, for example, this function would return true 80% of the time
function randomBool(odds)
{
	return Math.random() < odds
}
function saveAdvancedSettings()
{
	oddsOfHavingInitial = $('#odds-of-having-initial').length && $('#odds-of-having-initial').val() != "" ? eval($('#odds-of-having-initial').val()) : oddsOfHavingInitial
	oddsOfHavingFinal = $('#odds-of-having-final').length && $('#odds-of-having-final').val() != "" ? eval($('#odds-of-having-final').val()) : oddsOfHavingFinal
	oddsOfSingleCharacterInitial = $('#odds-of-single-character-initial').length && $('#odds-of-single-character-initial').val() != "" ? eval($('#odds-of-single-character-initial').val()) : oddsOfSingleCharacterInitial
	oddsOfSingleCharacterFinal = $('#odds-of-single-character-final').length && $('#odds-of-single-character-final').val() != "" ? eval($('#odds-of-single-character-final').val()) : oddsOfSingleCharacterFinal
	oddsOfSingleCharacterNucleus = $('#odds-of-single-character-nucleus').length && $('#odds-of-single-character-nucleus').val() != "" ? eval($('#odds-of-single-character-nucleus').val()) : oddsOfSingleCharacterNucleus
	oddsOfAddingAnotherSyllable = $('#odds-of-adding-another-syllable').length && $('#odds-of-adding-another-syllable').val() != "" ? eval($('#odds-of-adding-another-syllable').val()) : oddsOfAddingAnotherSyllable
	minimumNumberOfSyllables = $('#minimum-number-of-syllables').length && $('#minimum-number-of-syllables').val() != "" ? eval($('#minimum-number-of-syllables').val()) : minimumNumberOfSyllables
	maximumNumberOfSyllables = $('#maximum-number-of-syllables').length && $('#maximum-number-of-syllables').val() != "" ? eval($('#maximum-number-of-syllables').val()) : maximumNumberOfSyllables
	letters.initial = $('#initial').val().split( /,\s*/ )
	letters.final = $('#final').val().split( /,\s*/ )
	letters.nucleus = $('#nucleus').val().split( /,\s*/ )
	letters.initialMulti = $('#initial-multi').val().split( /,\s*/ )
	letters.finalMulti = $('#final-multi').val().split( /,\s*/ )
	letters.nucleusMulti = $('#nucleus-multi').val().split( /,\s*/ )
}
// returns true if test is true for each element in arr
function trueForAll(arr, test)
{
	for(var i = 0; i < arr.length; i++) if( !test(arr[i])) return false
	return true
}
// returns true if there was an error, false otherwise
function evaluateIfNoErrorsOtherwiseDisplayErrorMessage(errors, functionToEvaluate, errorAlertLocationName)
{
	for(var i = 0; i < errors.length; i++)
	{
		func = new Function("return " + errors[i].condition)
		if(func())
		{
			$(errorAlertLocationName).append("<p class='error-message'> " + errors[i].text + " </p>")
			return true
		}
	}
	functionToEvaluate()
	return false
}
function generateWords()
{
	for(var i = 0; i < numberOfWords; i++)
	{
		var word = ""
		for(var j = 0; j < determineNumberOfSyllables(); j++)
		{
			var randomInitial = randomBool(oddsOfHavingInitial) ? randomBool(oddsOfSingleCharacterInitial) ? letters.initial[Math.floor(Math.random() * letters.initial.length)] : letters.initialMulti[Math.floor(Math.random() * letters.initialMulti.length)] : "";
			var randomNucleus = randomBool(oddsOfSingleCharacterNucleus) ? letters.nucleus[Math.floor(Math.random() * letters.nucleus.length)] : letters.nucleusMulti[Math.floor(Math.random() * letters.nucleusMulti.length)];
			var randomFinal = randomBool(oddsOfHavingFinal) ? randomBool(oddsOfSingleCharacterFinal) ? letters.final[Math.floor(Math.random() * letters.final.length)] : letters.finalMulti[Math.floor(Math.random() * letters.finalMulti.length)] : "";
			word += randomInitial
			word += randomNucleus
			word += randomFinal
		}
		$("#coined-word-results").append("<p> " + word + " </p>")
	}
}